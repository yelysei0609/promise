class MyPromise {
  constructor(executor) {
    this.value = null;
    this.state = 'Pending';
    this.handlers = [];

    if (typeof executor !== 'function') {
      throw new TypeError();
    }
  }
}

const Promise = MyPromise;

module.exports = Promise;
